package com.example.gradlejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradlejpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(GradlejpaApplication.class, args);
    }

}
